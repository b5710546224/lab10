import static org.junit.Assert.*;

import org.junit.Assume;
import org.junit.Before;
import org.junit.Test;

import ku.util.Stack;
import ku.util.StackFactory;

public class StackTest {
	private Stack stack;
	private int capacity = 2;
	
	@Before
	public void setUp(){
		StackFactory.setStackType(1);
		stack = StackFactory.makeStack(capacity);
		
	}
	
	@Test
	public void newStackIsEmpty() {
		
		assertTrue(stack.isEmpty());
		assertFalse(stack.isFull());
		assertEquals(0,stack.size());
	}
	
	@Test(expected = java.util.EmptyStackException.class)
	public void testPopEmptyStack(){
		stack = StackFactory.makeStack(capacity);
		Assume.assumeTrue(stack.isEmpty());
		stack.pop();
	}
	
	@Test
	public void testPeek(){

		stack.push("cake");
		stack.push("cookies");
		Object food1 = stack.peek();
		Object food2 = stack.peek();
		assertSame(food1,food2);
		assertEquals(capacity,stack.size());
	}
	
	@Test
	public void testPushFirstElement(){
		stack.push("new");
		assertEquals(1,stack.size());
	}
	
	@Test
	public void testPop(){
		stack.push("cake");
		stack.push("cookies");

		String food1 = (String)stack.pop();
		String food2 = (String)stack.pop();
		assertNotSame(food1,food2);
		assertEquals(0,stack.size());
	}
	
	@Test
	public void testIsFull(){
		stack.push("cake");
		stack.push("cookies");
		assertTrue(stack.isFull());
	}
	
	@Test(expected = java.lang.IllegalStateException.class)
	public void testPushToFullStack(){
		stack.push("new1");
		stack.push("new2");
		stack.push("new3");
		stack.push("new4");
		stack.push("new5");
		stack.push("new6");		
	}
}
